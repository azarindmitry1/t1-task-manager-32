package ru.t1.azarin.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    public final static String NAME = "disconnect";

    @NotNull
    public final static String DESCRIPTION = "Disconnect from the server.";

    @Override
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
