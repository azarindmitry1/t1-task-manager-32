package ru.t1.azarin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-view-profile";

    @NotNull
    public final static String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest();
        @NotNull final User user = getAuthEndpoint().viewProfile(request).getUser();
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
