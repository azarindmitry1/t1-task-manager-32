package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataBase64LoadRequest;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-load-base64";

    @NotNull
    public final static String DESCRIPTION = "Load data from base64 file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest();
        getDomainEndpoint().base64LoadResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return null;
    }

    @Override
    public @Nullable String getDescription() {
        return null;
    }

}
