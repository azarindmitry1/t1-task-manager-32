package ru.t1.azarin.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.endpoint.IEndpointClient;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull
    public final static String NAME = "connect";

    @NotNull
    public final static String DESCRIPTION = "Connect to the server.";

    @Override
    public void execute() {
        try {
            @NotNull final IEndpointClient endpointClient = getServiceLocator().getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();
            getServiceLocator().getAuthEndpoint().setSocket(socket);
            getServiceLocator().getSystemEndpoint().setSocket(socket);
            getServiceLocator().getDomainEndpoint().setSocket(socket);
            getServiceLocator().getProjectEndpoint().setSocket(socket);
            getServiceLocator().getTaskEndpoint().setSocket(socket);
            getServiceLocator().getUserEndpoint().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
