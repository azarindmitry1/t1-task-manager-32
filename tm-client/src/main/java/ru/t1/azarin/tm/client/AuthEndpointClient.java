package ru.t1.azarin.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.azarin.tm.dto.response.user.UserLoginResponse;
import ru.t1.azarin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.azarin.tm.dto.response.user.UserViewProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserViewProfileResponse viewProfile(@NotNull final UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}
