package ru.t1.azarin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.azarin.tm.api.repository.ICommandRepository;
import ru.t1.azarin.tm.api.service.ICommandService;
import ru.t1.azarin.tm.api.service.ILoggerService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.IServiceLocator;
import ru.t1.azarin.tm.client.*;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.command.server.ConnectCommand;
import ru.t1.azarin.tm.command.server.DisconnectCommand;
import ru.t1.azarin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.azarin.tm.exception.system.CommandNotSupportedException;
import ru.t1.azarin.tm.repository.CommandRepository;
import ru.t1.azarin.tm.service.CommandService;
import ru.t1.azarin.tm.service.LoggerService;
import ru.t1.azarin.tm.service.PropertyService;
import ru.t1.azarin.tm.util.SystemUtil;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.azarin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final AuthEndpointClient authEndpoint = new AuthEndpointClient();

    @Getter
    @NotNull
    private final SystemEndpointClient systemEndpoint = new SystemEndpointClient();

    @Getter
    @NotNull
    private final DomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @Getter
    @NotNull
    private final ProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final TaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @Getter
    @NotNull
    private final UserEndpointClient userEndpoint = new UserEndpointClient();

    @Getter
    @NotNull
    private final ConnectionEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void connect() {
        processCommands(ConnectCommand.NAME);
    }

    private void disconnect() {
        processCommands(DisconnectCommand.NAME);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(param);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
        return true;
    }

    public void processCommands(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPid();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
        connect();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
        disconnect();
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);

        prepareStartup();

        while (true) {
            try {
                System.out.println("ENTER THE COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommands(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[ERROR]");
            }
        }
    }

}