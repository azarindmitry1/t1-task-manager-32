package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.endpoint.IEndpointClient;
import ru.t1.azarin.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

}
