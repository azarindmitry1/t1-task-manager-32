package ru.t1.azarin.tm.dto.request.user;

import lombok.NoArgsConstructor;
import ru.t1.azarin.tm.dto.request.AbstractRequest;

@NoArgsConstructor
public final class UserLogoutRequest extends AbstractRequest {
}
