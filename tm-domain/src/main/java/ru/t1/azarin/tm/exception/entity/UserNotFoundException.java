package ru.t1.azarin.tm.exception.entity;

public final class UserNotFoundException extends AbstractEntityException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
