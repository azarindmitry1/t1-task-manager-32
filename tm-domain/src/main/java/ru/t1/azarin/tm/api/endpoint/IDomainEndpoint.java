package ru.t1.azarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.data.*;
import ru.t1.azarin.tm.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse backupLoadResponse(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse backupSaveResponse(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse base64LoadResponse(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse base64SaveResponse(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse binaryLoadResponse(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse binarySaveResponse(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse jsonLoadFasterXmlResponse(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxbResponse jsonLoadJaxbResponse(@NotNull DataJsonLoadJaxbRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse jsonSaveFasterXmlResponse(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonSaveJaxbResponse jsonSaveJaxbResponse(@NotNull DataJsonSaveJaxbRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse xmlLoadFasterXmlResponse(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxbResponse xmlLoadJaxbResponse(@NotNull DataXmlLoadJaxbRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse xmlSaveFasterXmlResponse(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlSaveJaxbResponse xmlSaveJaxbResponse(@NotNull DataXmlSaveJaxbRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse yamlLoadFasterXmlResponse(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse yamlSaveFasterXmlResponse(@NotNull DataYamlSaveFasterXmlRequest request);

}
