package ru.t1.azarin.tm.dto.request.project;

import lombok.NoArgsConstructor;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;

@NoArgsConstructor
public final class ProjectClearRequest extends AbstractUserRequest {
}
