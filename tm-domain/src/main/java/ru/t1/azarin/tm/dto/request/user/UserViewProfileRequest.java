package ru.t1.azarin.tm.dto.request.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserViewProfileRequest extends AbstractUserRequest {
}
