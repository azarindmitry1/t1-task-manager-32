package ru.t1.azarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.user.*;
import ru.t1.azarin.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changePasswordResponse(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockResponse(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse registryResponse(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeResponse(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse unlockResponse(@NotNull UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfileResponse(@NotNull UserUpdateProfileRequest request);

}
