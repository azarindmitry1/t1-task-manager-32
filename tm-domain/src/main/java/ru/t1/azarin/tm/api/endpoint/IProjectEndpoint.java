package ru.t1.azarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.project.*;
import ru.t1.azarin.tm.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeStatusByIdResponse(
            @NotNull ProjectChangeStatusByIdRequest request
    );

    ProjectChangeStatusByIndexResponse changeStatusByIndexResponse(
            @NotNull ProjectChangeStatusByIndexRequest request
    );

    @NotNull
    ProjectClearResponse clearResponse(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCompleteByIdResponse completeByIdResponse(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse completeByIndexResponse(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    ProjectCreateResponse createResponse(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectListResponse listResponse(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeByIdResponse(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeByIndexResponse(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectShowByIdResponse showByIdResponse(@NotNull ProjectShowByIdRequest request);

    @NotNull
    ProjectShowByIndexResponse showByIndexResponse(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse startByIdResponse(@NotNull ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse startByIndexResponse(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateByIdResponse(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateByIndexResponse(@NotNull ProjectUpdateByIndexRequest request);

}
