package ru.t1.azarin.tm.dto.request.task;

import lombok.NoArgsConstructor;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;

@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {
}
