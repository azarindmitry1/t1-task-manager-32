package ru.t1.azarin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IEndpointClient {

    @Nullable
    Socket connect();

    @Nullable
    Socket disconnect();

}
