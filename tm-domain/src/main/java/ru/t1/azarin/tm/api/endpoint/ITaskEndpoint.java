package ru.t1.azarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.task.*;
import ru.t1.azarin.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindToProjectResponse(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeStatusByIndexResponse(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeStatusByIdResponse(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskClearResponse clearResponse(@NotNull TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse completeByIdResponse(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeByIndexResponse(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse createResponse(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListByProjectIdResponse listByProjectIdResponse(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    TaskListResponse listResponse(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeByIdResponse(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeByIndexResponse(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse showByIdResponse(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse showByIndexResponse(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskStartByIdResponse startByIdResponse(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startByIndexResponse(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindFromProjectResponse(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse updateByIdResponse(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateByIndexResponse(@NotNull TaskUpdateByIndexRequest request);

}
