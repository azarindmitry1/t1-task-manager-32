package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.IServiceLocator;
import ru.t1.azarin.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.azarin.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.azarin.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.azarin.tm.dto.response.system.ApplicationVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Override
    @NotNull
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setName(getPropertyService().getAuthorName());
        response.setEmail(getPropertyService().getAuthorEmail());
        return response;
    }

    @Override
    @NotNull
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(getPropertyService().getApplicationVersion());
        return response;
    }

}
