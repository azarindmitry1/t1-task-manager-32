package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.azarin.tm.api.service.IDomainService;
import ru.t1.azarin.tm.api.service.IServiceLocator;
import ru.t1.azarin.tm.dto.request.data.*;
import ru.t1.azarin.tm.dto.response.data.*;
import ru.t1.azarin.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return serviceLocator.getDomainService();
    }

    @NotNull
    @Override
    public DataBackupLoadResponse backupLoadResponse(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().backupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse backupSaveResponse(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().backupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse base64LoadResponse(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().base64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse base64SaveResponse(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().base64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse binaryLoadResponse(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().binaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse binarySaveResponse(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().binarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse jsonLoadFasterXmlResponse(@NotNull final DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().jsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @Override
    public DataJsonLoadJaxbResponse jsonLoadJaxbResponse(@NotNull final DataJsonLoadJaxbRequest request) {
        check(request, Role.ADMIN);
        getDomainService().jsonLoadJaxb();
        return new DataJsonLoadJaxbResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse jsonSaveFasterXmlResponse(@NotNull final DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().jsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxbResponse jsonSaveJaxbResponse(@NotNull final DataJsonSaveJaxbRequest request) {
        check(request, Role.ADMIN);
        getDomainService().jsonSaveJaxb();
        return new DataJsonSaveJaxbResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse xmlLoadFasterXmlResponse(@NotNull final DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().xmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxbResponse xmlLoadJaxbResponse(@NotNull final DataXmlLoadJaxbRequest request) {
        check(request, Role.ADMIN);
        getDomainService().xmlLoadJaxb();
        return new DataXmlLoadJaxbResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse xmlSaveFasterXmlResponse(@NotNull final DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().xmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxbResponse xmlSaveJaxbResponse(@NotNull final DataXmlSaveJaxbRequest request) {
        check(request, Role.ADMIN);
        getDomainService().xmlSaveJaxb();
        return new DataXmlSaveJaxbResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse yamlLoadFasterXmlResponse(@NotNull final DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().yamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse yamlSaveFasterXmlResponse(@NotNull final DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().yamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
