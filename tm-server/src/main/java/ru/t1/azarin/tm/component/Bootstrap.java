package ru.t1.azarin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.endpoint.*;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.*;
import ru.t1.azarin.tm.dto.request.data.*;
import ru.t1.azarin.tm.dto.request.project.*;
import ru.t1.azarin.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.azarin.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.azarin.tm.dto.request.task.*;
import ru.t1.azarin.tm.dto.request.user.*;
import ru.t1.azarin.tm.endpoint.*;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.repository.ProjectRepository;
import ru.t1.azarin.tm.repository.TaskRepository;
import ru.t1.azarin.tm.repository.UserRepository;
import ru.t1.azarin.tm.service.*;
import ru.t1.azarin.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusByIdResponse);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeStatusByIndexResponse);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearResponse);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeByIdResponse);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeByIndexResponse);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createResponse);
        server.registry(ProjectListRequest.class, projectEndpoint::listResponse);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeByIdResponse);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeByIndexResponse);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showByIdResponse);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showByIndexResponse);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startByIdResponse);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startByIndexResponse);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateByIdResponse);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndexResponse);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindToProjectResponse);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusByIdResponse);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndexResponse);
        server.registry(TaskClearRequest.class, taskEndpoint::clearResponse);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeByIdResponse);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeByIndexResponse);
        server.registry(TaskCreateRequest.class, taskEndpoint::createResponse);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listByProjectIdResponse);
        server.registry(TaskListRequest.class, taskEndpoint::listResponse);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeByIdResponse);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeByIndexResponse);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showByIdResponse);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showByIndexResponse);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startByIdResponse);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startByIndexResponse);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindFromProjectResponse);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateByIdResponse);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndexResponse);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePasswordResponse);
        server.registry(UserLockRequest.class, userEndpoint::lockResponse);
        server.registry(UserRegistryRequest.class, userEndpoint::registryResponse);
        server.registry(UserRemoveRequest.class, userEndpoint::removeResponse);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockResponse);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfileResponse);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::backupLoadResponse);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::backupSaveResponse);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::base64LoadResponse);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::base64SaveResponse);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::binaryLoadResponse);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::binarySaveResponse);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::jsonLoadFasterXmlResponse);
        server.registry(DataJsonLoadJaxbRequest.class, domainEndpoint::jsonLoadJaxbResponse);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::jsonSaveFasterXmlResponse);
        server.registry(DataJsonSaveJaxbRequest.class, domainEndpoint::jsonSaveJaxbResponse);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::xmlLoadFasterXmlResponse);
        server.registry(DataXmlLoadJaxbRequest.class, domainEndpoint::xmlLoadJaxbResponse);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::xmlSaveFasterXmlResponse);
        server.registry(DataXmlSaveJaxbRequest.class, domainEndpoint::xmlSaveJaxbResponse);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::yamlLoadFasterXmlResponse);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::yamlSaveFasterXmlResponse);
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        final User admin = userService.create("admin", "admin", Role.ADMIN);
        final User testUser = userService.create("test_user", "test_user", "test_user@test");

        projectService.add(admin.getId(), new Project("proj-one", "proj-one-desc"));
        projectService.add(admin.getId(), new Project("proj-two", "proj-two-desc"));
        projectService.add(admin.getId(), new Project("proj-three", "proj-three-desc"));

        taskService.add(admin.getId(), new Task("task-one", "task-desc-one"));
        taskService.add(admin.getId(), new Task("task-two", "task-desc-two"));
        taskService.add(admin.getId(), new Task("task-three", "task-desc-three"));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    public void run(@Nullable final String[] args) {
        initPid();
        initDemoData();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

}